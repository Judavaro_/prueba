package test;

import cucumber.api.CucumberOptions;
import net.serenitybdd.cucumber.CucumberWithSerenity;
import org.junit.runner.RunWith;

@RunWith(CucumberWithSerenity.class)
@CucumberOptions(features= {"src/test/resources/features/LoadCalculatorPage.feature"}, glue = {"definitions.load"})
public class LoadCalculatorPageTest {}