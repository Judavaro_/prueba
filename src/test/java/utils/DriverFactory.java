package utils;

import io.github.bonigarcia.wdm.WebDriverManager;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;

public class DriverFactory {

  public static WebDriver buildLocalWebDriver (){

    WebDriverManager.chromedriver().setup();
    ChromeOptions options = new ChromeOptions();
    options.addArguments("--kiosk");
    options.addArguments("window-size=1400,800");
    //options.addArguments("headless");
    return new ChromeDriver(options);

  }

}
