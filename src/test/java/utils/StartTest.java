package utils;

import java.net.HttpURLConnection;
import java.net.URL;
import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.junit.Assert;
import org.openqa.selenium.WebDriver;

public class StartTest {

  public static WebDriver driver;

  public static void startDriver (){

      try {

        driver = DriverFactory.buildLocalWebDriver();

      } catch (Exception e){
        System.out.println("Error: "+e.getMessage());
      }
  }

  public static void loadPage (){

    try {

      driver.get("https://www.metrocuadrado.com/calculadora-credito-hipotecario-vivienda/");

    } catch (Exception e){
      System.out.println("Error: "+e.getMessage());
    }
  }

  public static void checkTheCode(){

    try {

      HttpURLConnection.setFollowRedirects(false);
      HttpURLConnection conn = (HttpURLConnection) new URL(driver.getCurrentUrl()).openConnection();
      conn.setRequestMethod("HEAD"); // Se utiliza Head para solo obtener metadata
      DefaultHttpClient httpclient = new DefaultHttpClient();
      String urlString = conn.getURL().toString();

      HttpGet method = new HttpGet(urlString);
      HttpResponse response = httpclient.execute(method);
      response.getEntity().getContent();

      int statusCode = response.getStatusLine().getStatusCode();

      boolean band = true;

      if (statusCode == 200) {
        band = true;
      }
      if (statusCode >= 201 && statusCode <= 309) {
        band = false;
      }
      if (statusCode >= 400 && statusCode <= 505) {
        band = false;
      }

      //Interrumpe fallando la prueba cuando un código es diferente de 200 o cuando band es false
      Assert.assertTrue("Fatal Error: The page is not load correctly XD",band);

    } catch (Exception e) {
      System.out.print("Exception error: "+e.getMessage());
    }

  }

}
