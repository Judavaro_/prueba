package definitions.load;

import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import net.thucydides.core.annotations.Steps;
import steps.LoadCalculatorSteps;

public class LoadCalculatorPageStepDefinition {

  @Steps
  LoadCalculatorSteps loadCalculatorSteps;

  @Given("^the user open the browser$")
  public void the_user_open_the_browser() {
    loadCalculatorSteps.startTest();
  }

  @When("^the browser load the page url$")
  public void the_browser_load_the_page_url() {
    loadCalculatorSteps.browserLoadPage();
  }

  @Then("^the page will return the http code$")
  public void the_page_will_return_the_http_code() {
    loadCalculatorSteps.checkTheCode();
  }
}
