package steps;

import net.thucydides.core.annotations.Step;
import utils.StartTest;

public class LoadCalculatorSteps {

  private StartTest startTest;

  @Step
  public void startTest(){
    startTest.startDriver();
  }

  @Step
  public void browserLoadPage(){
    startTest.loadPage();
  }

  @Step
  public void checkTheCode(){
    startTest.checkTheCode();
  }

}
